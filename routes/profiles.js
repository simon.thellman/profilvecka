let fs = require('fs').promises;
let URL = require('url').URL;
let mongoClient = require('mongodb').MongoClient;

exports.processRoute = async function(request, response, route) {
    let dbConnection;
    try {
        let profileTemplate = await fs.readFile('./templates/profiles.html', {encoding: 'utf8'});
        
        let tmpURL = new URL('http://' + request.headers.host + request.url);
        
        dbConnection = await mongoClient.connect('mongodb://127.0.0.1:27017');
        let dbo = dbConnection.db('ProfVeckaDB');
        
        let dbres = await dbo.collection('profiles').find({}).toArray();
        if (dbres){
            let name = '';
            for (let i=0; i<dbres.length; i++ ) {
                name += '<a href="profile?profileid=';
                name += dbres[i].name;
                name += '">';
                name += dbres[i].name;
                name += '</a> <br> <br>';                
            }
            profileTemplate = profileTemplate.replace('{{name}}', name);
            
            response.writeHead(200, {'Content-Type': 'text/html'});
            response.end(profileTemplate);
        }
        else{
            response.writeHead(404, {'Content-Type': 'text/plain'});
            response.end('Could not find profile');
        }
    }
    catch (e){
        console.log(e.message);
        response.writeHead(500, {'Content-Type':'text/plain'});
        response.end('Error 500 - Internal server error');
    }
    finally{
        dbConnection.close();
    }
};

