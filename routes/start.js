let fs = require('fs').promises;

exports.processRoute = async function(request, response, route) {
    let startTemplate = await fs.readFile('./templates/start.html');
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.end(startTemplate);
};

