let profile = require('./routes/profile.js');
let start = require('./routes/start.js');
let profiles = require('./routes/profiles.js');
let createprofile = require('./routes/createprofile.js');

exports.processRoute = function(request, response, route){
    if (route.length === 0){
          start.processRoute(request, response, route);
    }
    else{
        let stage = route.shift();
        if (stage === 'profile'){
            profile.processRoute(request, response, route);
        }
        else if (stage === 'profiles') {
            profiles.processRoute(request, response, route);
        }
        else if (stage === 'createprofile') {
            createprofile.processRoute(request, response, route);
        }
        else {
            response.writeHead(404, {'Content-Type': 'text/plain'});
            response.end('Error 404 - Not found!');
        }
    }
    
};


