let fs = require('fs').promises;
let URL = require('url').URL;
let mongoClient = require('mongodb').MongoClient;


exports.processRoute = async function(request, response, route) {
    let dbconnection;
    
    if (request.method === 'POST'){
        let postdata = '';

        request.on('data', function(chunk){
           postdata += chunk;
        });

        request.on('end', async function(){
            let searchParams = new URLSearchParams(postdata);
        
        

    if (searchParams.has('name') && searchParams.has('profilepicture') && searchParams.has('description') && searchParams.has('gallery')){
    try {
    let galleryArray = searchParams.getAll('gallery');
    
    dbconnection = await mongoClient.connect('mongodb://127.0.0.1:27017');
    let dbo = dbconnection.db('ProfVeckaDB');
    
    let dbpost = {
    name: searchParams.get('name'),
    profilePicture: searchParams.get('profilepicture'),
    description: searchParams.get('description'),
    galleryImages: galleryArray
    };
 
    await dbo.collection('profiles').insertOne(dbpost);
    response.writeHead(303, {'Location': '/profile?profileid=' + searchParams.get('name')});
    response.end();
    
    }
    catch (e) {
        console.log(e.message);
        response.writeHead(500, {'Content-Type':'text/plain'});
        response.end('Error 500 - Internal server error');
    }
    finally{
        dbconnection.close();
    }
    }
    else {
        response.writeHead(422, {'Content-Type': 'text/plain'});
        response.end('Error - All fields was not provided');
    }
     });
 }
 else {
     let createProfileTemplate = await fs.readFile('./templates/createprofile.html', {encoding: 'utf8'});
     dbConnection = await mongoClient.connect('mongodb://127.0.0.1:27017');
     response.writeHead(200, {'Content-Type': 'text/html'});
     response.end(createProfileTemplate);
 }
};

