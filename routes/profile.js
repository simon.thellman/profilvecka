let fs = require('fs').promises;
let URL = require('url').URL;
let mongoClient = require('mongodb').MongoClient;

exports.processRoute = async function(request, response, route){
    let dbConnection;
    try{
        let profileTemplate = await fs.readFile('./templates/profile.html', {encoding: 'utf8'});
        
        let tmpURL = new URL('http://' + request.headers.host + request.url);
        
        dbConnection = await mongoClient.connect('mongodb://127.0.0.1:27017');
        let dbo = dbConnection.db('ProfVeckaDB');
        
        let dbres = await dbo.collection('profiles').findOne({ name: tmpURL.searchParams.get('profileid')});
        if (dbres){
            profileTemplate = profileTemplate.replace('{{name}}', dbres.name);
            profileTemplate = profileTemplate.replace('{{profilepicture}}', dbres.profilePicture);
            profileTemplate = profileTemplate.replace('{{description}}', dbres.description);
            let imgGallery = '';
            for (let i=0; i<dbres.galleryImages.length; i++){
                imgGallery += '<img src="';
                imgGallery += dbres.galleryImages[i];
//                imgGallery += '" alt="';
//                imgGallery += dbres.galleryImages[i].description;
                imgGallery += '">';
            }
            profileTemplate = profileTemplate.replace('{{galleryImages}}', imgGallery);
                
            
            response.writeHead(200, {'Content-Type': 'text/html'});
            response.end(profileTemplate);
        }
        else {
            response.writeHead(404, {'Content-Type': 'text/plain'});
            response.end('Could not find profile');
        }

    }
    catch(e){
        console.log(e.message);
        response.writeHead(500, {'Content-Type':'text/plain'});
        response.end('Error 500 - Internal server error');
        
    }
    finally{
        dbConnection.close();
    }
};


